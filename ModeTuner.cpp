#include "ModeTuner.h"

using namespace std;

ModeTuner::ModeTuner(DigitDisplay* display, HxStomp* hx) {
  this->hx = hx; 
  this->display = display;
  this->setup();
}

void ModeTuner::setup() {}

bool ModeTuner::changeRequested(Event* event) {
  return event->longPress && event->firstPressed == 1 && !event->doublePress;
}

void ModeTuner::onEnter(Event* event) {
  this->hx->tuner();
}

void ModeTuner::onExit(Event* event) {
  this->hx->tuner();
}

bool ModeTuner::onEvent(Event* event) {
  return false;
}

void ModeTuner::show() {
  this->display->letter('U');
  this->display->led(false);
}

ControllerMode ModeTuner::mode() {
  return ControllerMode::TUNER;
}

void ModeTuner::loop() {}

