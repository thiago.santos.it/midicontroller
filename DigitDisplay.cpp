#include "DigitDisplay.h"

using namespace std;

DigitDisplay::DigitDisplay(byte *digitPins, byte ledPin) {
  this->digitPins = digitPins;
  this->ledPin = ledPin;
  this->setup();
}

void DigitDisplay::setup() {
  for (byte position = 0; position < DIGIT_SEGMENTS; position++) { 
    pinMode(this->digitPins[position], OUTPUT); 
  }
  pinMode(this->ledPin, OUTPUT); 
}

void DigitDisplay::led(boolean state) {
  digitalWrite(this->ledPin, !state);
}

bool DigitDisplay::number(byte number) {
  if (number < 0 || number > 9) { return false; }
  this->segments(this->ledMap[number]);
  return true;    
}

bool DigitDisplay::letter(char letter) {
  bool result = false;
  if (letter == ' ') {  
    this->segments(this->ledMap[10]);
    result = true;
  } else if (letter == 'U') { 
    this->segments(this->ledMap[11]);
    result = true;
  }
  return result;    
}

void DigitDisplay::segments(byte states[DIGIT_SEGMENTS]) {
  for (byte position = 0; position < DIGIT_SEGMENTS; position++) {
    digitalWrite(this->digitPins[position], !states[position]);
  }
}

void DigitDisplay::clear() {
  for (byte position = 0; position < DIGIT_SEGMENTS; position++) { 
    pinMode(this->digitPins[position], LOW); 
  }
}

bool DigitDisplay::initialized() {
  return this->digitPins == NULL;
}

