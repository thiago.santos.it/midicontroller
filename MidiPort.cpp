#include "MidiPort.h"

//Reference tutorial: <https://ccrma.stanford.edu/~craig/articles/linuxmidi/misc/essenmidi.html>
#define CMD_NOTE_OFF 0x80
#define CMD_NOTE_ON 0x90
#define CMD_AFTERTOUCH 0xA0
#define CMD_CONTINUOUS_CONTROLLER 0xB0
#define CMD_PATCH_CHANGE 0xC0
#define CMD_CHANNEL_PRESSURE 0xD0
#define CMD_PITCH_BEND 0xE0
#define CMD_NON_MUSICAL 0xF0

using namespace std;

MidiPort::MidiPort(int channel) {
  this->channel = channel;
  this->setup();
}

void MidiPort::setup() {}

void MidiPort::sendCC(int param, int value) {
  Log.notice("MIDI: Channel %d - CC %d,%d" CR, this->printableChannel(), param, value);
  this->send(CMD_CONTINUOUS_CONTROLLER, param, value);
}

void MidiPort::sendPC(int param, int value) {
  Log.notice("MIDI: Channel %d - PC %d,%d" CR, this->printableChannel(), param, value);
  this->send(CMD_PATCH_CHANGE, param, value);
}

void MidiPort::sendNoteOn(int param, int value) {
  Log.notice("MIDI: Channel %d - Note On %d,%d" CR, this->printableChannel(), param, value);
  this->send(CMD_NOTE_ON, param, value);
}

void MidiPort::sendNoteOff(int param, int value) {
  Log.notice("MIDI: Channel %d - Note Off %d,%d" CR, this->printableChannel(), param, value);
  this->send(CMD_NOTE_OFF, param, value);
}

void MidiPort::sendAftertouch(int param, int value) {
  Log.notice("MIDI: Channel %d - Aftertouch %d,%d" CR, this->printableChannel(), param, value);
  this->send(CMD_AFTERTOUCH, param, value);
}

void MidiPort::sendChannelPressure(int value) {
  Log.notice("MIDI: Channel %d - Channel Pressure %d,%d" CR, this->printableChannel(), value);
  this->send(CMD_CHANNEL_PRESSURE, value, 0);
}

void MidiPort::sendPitchBend(int param, int value) {
  Log.notice("MIDI: Channel %d - PitchBend %d,%d" CR, this->printableChannel(), param, value);
  this->send(CMD_PITCH_BEND, param, value);
}

void MidiPort::sendNonMusical(int param, int value) {
  Log.notice("MIDI: Channel %d - NonMusical %d,%d" CR, this->printableChannel(), param, value);
  this->send(CMD_NON_MUSICAL, param, value);
}

int MidiPort::printableChannel() {
  return this->channel + 1;
}

void MidiPort::send(int command, int param, int value) {
  Serial.write(command | this->channel);
  Serial.write(param);  
  Serial.write(value);
}
