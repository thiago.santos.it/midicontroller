/* 
    Looper mode impl
    Created by Thiago Medeiros dos Santos 
*/
#include "Arduino.h"
#include "SwitchControls.h"
#include "HxStomp.h"
#include "DigitDisplay.h"
#include "ModeBase.h"
#include "Log.h"

#define MAX_RECORDING_TIME_IN_MILLIS 29900U 

#ifndef ModeLooper_h
#define ModeLooper_h

class ModeLooper: public ModeBase {
 
  public: 
    ModeLooper(DigitDisplay* display, HxStomp* hx);
    ControllerMode mode();
    bool changeRequested(Event* event);
    void onEnter(Event* event);
    void onExit(Event* event);
    bool onEvent(Event* event); 
    void show();
    void loop();

  private:
    DigitDisplay* display = NULL; 
    HxStomp* hx = NULL;

    unsigned long recordingStartedAt = 0U;

    bool looperPlaying = false;
    bool looperRecording = false; 
    bool looperOverdub = false; 
    bool looperHasUndo = false; 
    bool looperReverse = false;
    bool looperHalfSpeed = false;

    void setup();
};

#endif