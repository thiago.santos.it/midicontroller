#include "ModePatchSelector.h"
#include <EEPROM.h>

#define EEPROM_ADDRESS 0
#define MIN_TIME_TO_SAVE_ON_EEPROM 60000U

using namespace std;

ModePatchSelector::ModePatchSelector(DigitDisplay* display) {
  this->display = display;
  this->patch = EEPROM.read(EEPROM_ADDRESS);
  Log.notice("EEPROM patch read address: %d, value: %d" CR, EEPROM_ADDRESS, this->patch);
  if (this->patch < 0 || this->patch > MAX_PATCH) { this->patch = 0;}
  this->setup();
}

void ModePatchSelector::setup() {}

bool ModePatchSelector::changeRequested(Event* event) {
  return event->doublePress && !event->longPress;
}

void ModePatchSelector::onEnter(Event* event) {}

void ModePatchSelector::onExit(Event* event) {
  EEPROM.update(EEPROM_ADDRESS, this->patch);
  Log.notice("EEPROM patch update address: %d, value: %d" CR, EEPROM_ADDRESS, this->patch);
}

bool ModePatchSelector::onEvent(Event* event) {
  byte initialPatch = this->patch;
  if (event->longPress || event->doublePress) { return false; }
  if (event->firstPressed == 1) { this->patch++; } else { patch--; }
  if (this->patch > MAX_PATCH) { this->patch = 0; }
  if (this->patch < 0) { this->patch = MAX_PATCH; }

  return true;
}

ControllerMode ModePatchSelector::mode() {
  return ControllerMode::PATCH_SELECTOR;
}

byte ModePatchSelector::currentPatch() {
  return this->patch;
}

void ModePatchSelector::show() {
  this->display->number(this->patch);
  this->display->led(true);
}

void ModePatchSelector::loop() {}
