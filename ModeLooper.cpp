#include "ModeLooper.h"

using namespace std;

ModeLooper::ModeLooper(DigitDisplay* display, HxStomp* hx) {
  this->display = display;
  this->hx = hx;
  this->setup();
}

void ModeLooper::setup() {}

ControllerMode ModeLooper::mode() {
  return ControllerMode::LOOPER;
}

bool ModeLooper::changeRequested(Event* event) {
  return event->longPress && event->doublePress;
}

void ModeLooper::onEnter(Event* event) {
    this->hx->looperRecord();
    this->recordingStartedAt= millis();
    this->looperPlaying = false;
    this->looperRecording = true; 
    this->looperOverdub = false; 
    this->looperHasUndo = false; 
    this->looperReverse = false;
    this->looperHalfSpeed = false;
    Log.notice("Recording" CR);
}

void ModeLooper::onExit(Event* event) {
  this->hx->looperStop();
  Log.notice("Stop" CR);
}

bool ModeLooper::onEvent(Event* event) {
  if (this->changeRequested(event)) { 
    Log.notice("changeRequested" CR);
    return false; 
  }

  if (this->looperRecording && event->longPress && event->firstPressed == 2 && !event->doublePress) {
    if (this->looperOverdub) { 
      if (this->looperHasUndo) {
        this->looperHasUndo = false;
        this->hx->looperRedo();
        Log.notice("Redo" CR);
      } else {
        this->looperHasUndo = true;
        this->hx->looperUndo();
        Log.notice("Undo" CR);
      }
    } else {
      this->looperOverdub = false;
      this->looperRecording = false;
      this->recordingStartedAt = 0U;
      this->hx->looperStop();
      Log.notice("Clear" CR);
    }
  } else if (!event->longPress && event->firstPressed == 1 && !event->doublePress) { 
    this->looperRecording = false;
    this->looperOverdub = false;
    if (!this->looperPlaying) {
      this->looperPlaying = true;
      this->hx->looperPlay();
      Log.notice("Play" CR);
    } else {
      this->looperPlaying = false;
      this->hx->looperStop();
      Log.notice("Stop" CR);
    }
  } else if (!this->looperRecording && event->longPress && event->firstPressed == 2 && !event->doublePress) {
    if (this->looperPlaying) {
      this->looperPlaying = false;
      this->hx->looperStop();
    }
    Log.notice("Stop if needed and clear" CR);
    this->recordingStartedAt = 0U;
  } else if (!event->longPress && event->firstPressed == 2 && !event->doublePress) {
    
    if (this->recordingStartedAt == 0U) {
      this->looperRecording = true;
      this->recordingStartedAt = millis();
      this->hx->looperRecord();
      Log.notice("Record" CR);
    } else {
      this->looperRecording = true;
      this->looperOverdub = true;
      this->hx->looperOverdub();
      Log.notice("Overdub" CR);
    }
  } else if (!event->longPress && event->doublePress) { 

    if (this->looperReverse) {
      this->looperReverse = false;
      this->hx->looperForward();
      Log.notice("Forward" CR);
    } else{
      this->looperReverse = true;
      this->hx->looperReverse();
      Log.notice("Reverse" CR);
    }
  } else if (event->longPress && event->firstPressed == 1 && !event->doublePress) {
    if (this->looperHalfSpeed) {
      this->looperHalfSpeed = false;
      this->hx->looperFullSpeed();
      Log.notice("Full speed" CR);
    } else{
      this->looperHalfSpeed = true;
      this->hx->looperHalfSpeed();
      Log.notice("Half speed" CR);
    }
  }
  return true;
} 

void ModeLooper::show() {
  if (this->looperRecording) {

    byte animate[DIGIT_SEGMENTS] = { 0,0,0,0,0,0,0 };
    int position = (int) millis() % 1200U / 200U;
    animate[position ] = 1;
    display->segments(animate);

  } else if (this->looperPlaying) {

    if (this->looperHalfSpeed) {
      byte on[DIGIT_SEGMENTS] = { 1,1,0,0,0,1,0 };
      display->segments(on);
    } else {
      byte on[DIGIT_SEGMENTS] = { 1,1,0,0,0,1,1 };
      display->segments(on);
    }
  } else {
    byte off[DIGIT_SEGMENTS] = { 0,0,1,1,1,0,1 };
    display->segments(off);
  }

  display->led(this->looperHalfSpeed || this->looperOverdub);
}

void ModeLooper::loop() {
  if (this->looperRecording && !this->looperOverdub && (millis() - this->recordingStartedAt) >= MAX_RECORDING_TIME_IN_MILLIS) {
    this->looperRecording = true;
    this->looperOverdub = true;
    this->hx->looperOverdub();
    Log.notice("More than 30 seg. Forced overdub!" CR);
  }
}
