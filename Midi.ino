#include "DigitDisplay.h"
#include "SwitchControls.h"
#include "MidiPort.h"
#include "HxStomp.h"
#include "System.h"
#include "Log.h"

#include "ModeBase.h"
#include "ModePatchSelector.h"
#include "ModeCommandMap.h"
#include "ModeTuner.h"
#include "ModeLooper.h"

#define MIDI_SERIAL_BAUD_RATE 31250
#define LOG_SERIAL_BAUD_RATE 9600
#define BAUD_RATE MIDI_SERIAL_BAUD_RATE

#define NUM_OF_SWITCHES 2
#define NUM_OF_MODES 4
#define SHORT_PRESS_TIME_IN_MILLIS 100U
#define LONG_PRESS_TIME_IN_MILLIS 1000U
#define MIN_LOOP_TIME_IN_MILLIS 5

#define NUM_OF_SWITCHES 2
#define SWITCH_PINS { 12, 11 }
#define DEFAULT_DIGIT_PINS { 2, 3, 4, 5, 6, 7, 8 }
#define DEFAULT_LED_PIN 9

byte controlPins[NUM_OF_SWITCHES] = SWITCH_PINS;
byte displayPins[DIGIT_SEGMENTS] = DEFAULT_DIGIT_PINS;

DigitDisplay *display = NULL;
SwitchControls *controls = NULL;
MidiPort *midi = NULL;
HxStomp *hx = NULL;

ModeBase* modes[NUM_OF_MODES] = { NULL };
ModeBase *currentMode = NULL;

void setup() {
  Serial.begin(BAUD_RATE);

  while(!Serial && !Serial.available()){}
  Log.begin(LOG_LEVEL_VERBOSE, &Serial);

  Log.notice("Starting MIDI Controller" CR);

  controls = new SwitchControls(controlPins, NUM_OF_SWITCHES, SHORT_PRESS_TIME_IN_MILLIS, LONG_PRESS_TIME_IN_MILLIS);
  midi = new MidiPort(CHANNEL_2);
  display = new DigitDisplay(displayPins, DEFAULT_LED_PIN);
  hx = new HxStomp(midi);

  modes[0] = new ModePatchSelector(display);
  modes[1] = new ModeCommandMap(display, midi, hx, modes[0]);
  modes[2] = new ModeTuner(display, hx);
  modes[3] = new ModeLooper(display, hx);
  
  currentMode = modes[1];
  Log.notice("MIDI Controller Started Sucessfully" CR);
}

void loop() {
 
  if (display == NULL) { return; }
  if (controls == NULL) { return; }
  if (hx == NULL) { return; }
  
  Event* event = controls->loop();
  if (event != NULL) {  

    Log.notice("Event received first: %d, last: %d, double: %b, long: %b" CR, 
        event->firstPressed, event->lastPressed, event->doublePress, event->longPress);

    bool hasEvent = currentMode->onEvent(event);
    if (!hasEvent) {
      ModeBase *initialCurrentMode = currentMode;
      for (byte position = 0; position < NUM_OF_MODES; position++) {
        if (currentMode == initialCurrentMode) {
          currentMode = modes[position]->changeModeIfNeeded(event, currentMode); 
        }   
      }
      Log.notice("Current mode: %d", currentMode->mode());
    }  
  }
  if (!animatePress()) {
    currentMode->show();
  }
  currentMode->loop();
  delay(MIN_LOOP_TIME_IN_MILLIS);
  delete event;
}

bool animatePress() {  
  bool result = false;
  byte commandSegments[DIGIT_SEGMENTS] = { 0,0,0,0,0,0,0 };
   if (controls->isPressed(1)) {
    controls->isLongPress() ? commandSegments[1] = 1 :  commandSegments[2] = 1;
  }
  if (controls->isPressed(2)) {
    controls->isLongPress() ? commandSegments[5] = 1 :  commandSegments[4] = 1;
  }
  if (controls->isPressed(1) || controls->isPressed(2)) {
    display->segments(commandSegments);
    result = true;
  } 
  return result;
}
