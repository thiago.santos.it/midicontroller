/* 
    Command map mode impl
    Created by Thiago Medeiros dos Santos 
*/
#include "Arduino.h"
#include "SwitchControls.h"
#include "ModeBase.h"
#include "ModePatchSelector.h"
#include "MidiPort.h"
#include "DigitDisplay.h"
#include "HxStomp.h"
#include "Log.h"

#ifndef ModeCommandMap_h
#define ModeCommandMap_h

class ModeCommandMap: public ModeBase {
 
  public: 
    ModeCommandMap(DigitDisplay* display, MidiPort* midi, HxStomp* hx, ModePatchSelector* patchMode);
    ControllerMode mode();
    bool changeRequested(Event* event);
    void onEnter(Event* event);
    void onExit(Event* event);
    bool onEvent(Event* event); 
    void show();
    void loop();
    
  private:
    ModePatchSelector* patchMode = NULL;
    HxStomp* hx = NULL;
    DigitDisplay* display = NULL;
    MidiPort* midi = NULL;
    
    bool middlePatch = false;

    void setup();
};

#endif