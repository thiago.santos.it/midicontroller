
/* 
    Multiple switch control for MIDI controller
    Created by Thiago Medeiros dos Santos 
*/
#include "Arduino.h"

#ifndef SwitchControls_h
#define SwitchControls_h

#define DEFAULT_SHORT_PRESS 500U
#define DEFAULT_LONG_PRESS 3000U

class Event {
  public: 
    byte firstPressed = 0;
    byte lastPressed = 0;
    bool doublePress = false;
    bool longPress = false;
    
    Event(byte firstPressed, byte lastPressed, bool doublePress, bool longPress) {
      this->firstPressed = firstPressed;
      this->lastPressed = lastPressed;
      this->doublePress = doublePress;
      this->longPress = longPress;
    }
};

class SwitchControls {
 
  public: 

    SwitchControls(byte *pins, byte numOfSwitches, unsigned long shortPressMinTimeInMillis = DEFAULT_SHORT_PRESS, unsigned long longPressMinTimeInMillis = DEFAULT_LONG_PRESS);
    Event* loop();
    bool isLongPress();
    bool isPressed(byte button);
  private:
    bool longPressTriggered = false;
    byte numOfSwitches = 0;
    byte pressedButtons = 0;
    byte maxSimutaniousButtons = 0;
    byte firstPressed = 0;
    byte lastPressed = 0;

    byte *pins = NULL;

    unsigned long *startedAt = NULL;
    unsigned long longPressMinTimeInMillis = -1; 
    unsigned long shortPressMinTimeInMillis = -1; 

    void setup();
    Event* released(byte position, unsigned long elapsedTime);
    void pressed(byte position, unsigned long currentTime);
};

#endif
