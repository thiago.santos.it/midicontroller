
/* 
    Single digit display for MIDI controller
    Created by Thiago Medeiros dos Santos 
*/
#include "Arduino.h"

#ifndef DigitDisplay_h
#define DigitDisplay_h

#define DIGIT_SEGMENTS 7
#define NUM_OF_DIGITS 12

#define NUMBER_0 { 1,1,1,1,1,1,0 }
#define NUMBER_1 { 0,1,1,0,0,0,0 }
#define NUMBER_2 { 1,1,0,1,1,0,1 }
#define NUMBER_3 { 1,1,1,1,0,0,1 }
#define NUMBER_4 { 0,1,1,0,0,1,1 }
#define NUMBER_5 { 1,0,1,1,0,1,1 }
#define NUMBER_6 { 1,0,1,1,1,1,1 }
#define NUMBER_7 { 1,1,1,0,0,0,0 }
#define NUMBER_8 { 1,1,1,1,1,1,1 }
#define NUMBER_9 { 1,1,1,1,0,1,1 }
#define LETTER_SPACE { 0,0,0,0,0,0,0 }
#define LETTER_U { 0,1,1,1,1,1,0 }

class DigitDisplay {

  public: 
    DigitDisplay(byte *digitPins, byte ledPin);
    void led(boolean state);
    bool number(byte number);
    bool letter(char letter);
    void segments(byte states[DIGIT_SEGMENTS]);
    void clear();
    bool initialized();

  private:
    byte ledPin;
    byte *digitPins = NULL;
    byte ledMap[NUM_OF_DIGITS][DIGIT_SEGMENTS] = { 
      NUMBER_0, 
      NUMBER_1, 
      NUMBER_2, 
      NUMBER_3, 
      NUMBER_4, 
      NUMBER_5, 
      NUMBER_6, 
      NUMBER_7, 
      NUMBER_8, 
      NUMBER_9,
      LETTER_SPACE,
      LETTER_U, 
    };
    void setup();
};

#endif
