/* 
    Interface like for modes
    Created by Thiago Medeiros dos Santos 
*/
#include "Arduino.h"
#include "SwitchControls.h"
#include "Log.h"

#ifndef ModeBase_h
#define ModeBase_h

enum ControllerMode {
  COMMAND_MAP,
  PATCH_SELECTOR, 
  TUNER, 
  LOOPER
};

class ModeBase {
  public: 

    ModeBase* changeModeIfNeeded(Event* event, ModeBase* currentMode);  
    
    virtual ControllerMode mode() = 0;
    virtual bool changeRequested(Event* event) = 0;
    virtual void onEnter(Event* event) = 0;
    virtual void onExit(Event* event) = 0;
    virtual bool onEvent(Event* event) = 0; 
    virtual void show() = 0;
    virtual void loop() = 0;
  private:
  
    ModeBase* previousMode;
};

#endif
