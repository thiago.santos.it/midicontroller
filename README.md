




# Mode description

## Command map

Just execute a series of simple MIDI command based on current patch. 

Patch 0: Pedal patch up and down
Patch 1: Snapshot up and down
Patch 2: Pedal mode next and previous
Patch 3: Bypass all on and off (any switch)
Patch 4: CC 104 and 114
Patch 5: CC 105 and 115
Patch 6: CC 106 and 116
Patch 7: CC 107 and 117
Patch 8: CC 108 and 118
Patch 9: Exp1 and exp 2

Press and hold both buttons goto patch 50 and goto patch 0

