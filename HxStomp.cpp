#include "HxStomp.h"

using namespace std;

HxStomp::HxStomp(MidiPort* midi) {
  this->midi = midi;
  this->setup();
}

void HxStomp::setup() {}


void HxStomp::pressExp1() {
  midi->sendCC(1, 0);
}

void HxStomp::pressExp2() {
  midi->sendCC(2, 0);
}

void HxStomp::pressFS1() {
  midi->sendCC(49, 0);
}
 
void HxStomp::pressFS2() {
  midi->sendCC(50, 0);
}
 
void HxStomp::pressFS3() {
  midi->sendCC(51, 0);
}
 
void HxStomp::pressFS4() {
  midi->sendCC(52, 0);
}
 
void HxStomp::pressFS5() {
  midi->sendCC(53, 0);
}

void HxStomp::looperRecord() {
  midi->sendCC(60, 127);
}

void HxStomp::looperOverdub() {
  midi->sendCC(60, 0);
}
 
void HxStomp::looperPlay() {
  midi->sendCC(61, 127);
}

void HxStomp::looperStop() {
  midi->sendCC(61, 0);
}
 
void HxStomp::looperPlayOnce() {
  midi->sendCC(62, 127);
}
 
void HxStomp::looperUndo() {
  midi->sendCC(63, 64);
}
 
 void HxStomp::looperRedo() {
  midi->sendCC(63, 127);
}

void HxStomp::looperReverse() {
  midi->sendCC(65, 127);
}

void HxStomp::looperForward() {
  midi->sendCC(65, 0);
}
 
void HxStomp::looperFullSpeed() {
  midi->sendCC(66, 0);
}

void HxStomp::looperHalfSpeed() {
  midi->sendCC(66, 127);
}
 
void HxStomp::tapTempo() {
  midi->sendCC(64, 127);
}
 
void HxStomp::tuner() {
  midi->sendCC(68, 0);
}

void HxStomp::snapshot(int num) {
  if (num >= 0 && num <= 2) {
    midi->sendCC(69, num);
  }
}

void HxStomp::nextSnapshot() {
  midi->sendCC(69, 8);
}

void HxStomp::previousSnapshot() {
  midi->sendCC(69, 9);
}

void HxStomp::bypassAll() {
  midi->sendCC(70, 0);
}

void HxStomp::bypassOff() {
 midi->sendCC(70, 127);
}

void HxStomp::footswitchMode(int num) {
  if (num >= 0 && num <= 3) {
    midi->sendCC(71, num);
  }
}

void HxStomp::footswitchModeNext() {
  midi->sendCC(71, 4);
}

void HxStomp::footswitchModePrevious() {
  midi->sendCC(71, 5);
}
