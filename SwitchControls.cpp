#include "Arduino.h"
#include "SwitchControls.h"
#include "Log.h"

#define DISABLE_LOGGING

#define PIN_ON 0
#define PIN_OFF 1

using namespace std;

SwitchControls::SwitchControls(byte *pins, byte numOfSwitches, unsigned long shortPressMinTimeInMillis, unsigned long longPressMinTimeInMillis) {

  this->pins = pins;
  this->startedAt = new unsigned long[numOfSwitches];
  this->numOfSwitches = numOfSwitches;

  this->shortPressMinTimeInMillis = shortPressMinTimeInMillis;
  this->longPressMinTimeInMillis = longPressMinTimeInMillis;

  this->setup();
}

void SwitchControls::setup() {
  for (byte position = 0; position < this->numOfSwitches; position++) {

    Log.notice("Switch %d inititalized at pin: %d" CR, position, this->pins[position]);

    this->startedAt[position] = 0U;
    pinMode(this->pins[position], INPUT);
    digitalWrite(this->pins[position], HIGH);
  }
}

Event* SwitchControls::loop() {

  Event* event = NULL;

  unsigned long currentTime = millis();
 
  for (int position = 0; position < this->numOfSwitches; position++) {
      int pin = digitalRead(this->pins[position]);
       
      if (pin == PIN_ON && this->startedAt[position] == 0U) {
         this->pressed(position, currentTime);
      } 

      unsigned long elapsedTime = currentTime - this->startedAt[position];
      
      this->longPressTriggered = this->longPressTriggered || (this->startedAt[position] > 0U && elapsedTime > this->longPressMinTimeInMillis);
    
      if (pin == PIN_OFF && this->startedAt[position] > 0U && elapsedTime > this->shortPressMinTimeInMillis) {
          this->pressedButtons--;
          this->startedAt[position] = 0U;
      }
      if (this->pressedButtons == 0 && this->firstPressed > 0) {
        event = this->released(position, elapsedTime);
      }
  }
  return event;
}

void SwitchControls::pressed(byte position, unsigned long currentTime) { 
     
  this->startedAt[position] = currentTime - 1;
    
  this->pressedButtons++;
  
  this->maxSimutaniousButtons = max(this->pressedButtons, this->maxSimutaniousButtons);

  if (this->firstPressed > 0) {
    this->lastPressed = position + 1;
  } else {
    this->firstPressed = position + 1;
  }
  Log.notice("Switch %d Pressed - Started at: %u" CR, position, this->startedAt[position]);
}

Event* SwitchControls::released(byte position, unsigned long elapsedTime) {
  Event* event = new Event(this->firstPressed, this->lastPressed, this->maxSimutaniousButtons > 1, this->longPressTriggered);

  this->longPressTriggered = false;
  this->maxSimutaniousButtons = 0;
  this->lastPressed = 0;
  this->firstPressed = 0;

  Log.notice("Switch %d Released - Started at : %u, after: %u, max btns: %d, long: %b" CR, position, this->startedAt[position], elapsedTime, this->maxSimutaniousButtons, this->longPressTriggered);
  
  return event;
}

bool SwitchControls::isLongPress() {
  return this->longPressTriggered;
}

bool SwitchControls::isPressed(byte button) {
  return this->firstPressed == button || this->lastPressed == button;
}
