#include "ModeBase.h"

using namespace std;

ModeBase* ModeBase::changeModeIfNeeded(Event* event, ModeBase* currentMode) {
  bool alreadyInMode = (currentMode->mode() == this->mode());

  if (alreadyInMode && this->changeRequested(event)) { 
    this->onExit(event);
    this->previousMode->onEnter(event);
    return this->previousMode;
  } 

  bool modeChanged = (currentMode->mode() != this->mode());
  if (modeChanged && this->changeRequested(event)) {
    currentMode->onExit(event);
    this->onEnter(event);
    this->previousMode = currentMode;
    return this;
  }
  return currentMode;
}