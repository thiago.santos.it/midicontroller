#include "System.h"

using namespace std;

extern unsigned int __bss_end;
extern unsigned int __heap_start;
extern void *__brkval;

System::System() {
  this->setup();
}

void System::setup() {}

int System::freeMemoryInBytes() {
	int free_memory;
	if ((int) __brkval) {
		return ((int) &free_memory) - ((int) __brkval);
  } else {
	  return ((int) &free_memory) - ((int) &__bss_end);
  }
}
