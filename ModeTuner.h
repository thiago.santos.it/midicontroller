/* 
    Tuner mode impl
    Created by Thiago Medeiros dos Santos 
*/
#include "Arduino.h"
#include "SwitchControls.h"
#include "HxStomp.h"
#include "DigitDisplay.h"
#include "ModeBase.h"
#include "Log.h"

#ifndef ModeTuner_h
#define ModeTuner_h

class ModeTuner: public ModeBase {
 
  public: 
    ModeTuner(DigitDisplay* display, HxStomp* hx);
    ControllerMode mode();
    bool changeRequested(Event* event);
    void onEnter(Event* event);
    void onExit(Event* event);
    bool onEvent(Event* event); 
    void show();
    void loop();
  private:
    HxStomp* hx = NULL;
    DigitDisplay* display = NULL;
    void setup();
};

#endif