#include "ModeCommandMap.h"

using namespace std;

ModeCommandMap::ModeCommandMap(DigitDisplay* display, MidiPort* midi, HxStomp* hx, ModePatchSelector* patchMode) {
  this->patchMode = patchMode;
  this->display = display;
  this->hx = hx;
  this->midi = midi;
  this->setup();
}

void ModeCommandMap::setup() {}

ControllerMode ModeCommandMap::mode() {
  return ControllerMode::COMMAND_MAP;
}

bool ModeCommandMap::changeRequested(Event* event) {
  return false;
}
void ModeCommandMap::onEnter(Event* event) {}

void ModeCommandMap::onExit(Event* event) {}

bool ModeCommandMap::onEvent(Event* event) {
 if (event->longPress && event->firstPressed == 2 && !event->doublePress) {
    this->midi->sendPC(0, this->middlePatch ? 0 : 57);
    this->middlePatch = !this->middlePatch;
    return true;
  } else if (event->longPress || event->doublePress) { 
    return false;
  }
  
  if (this->patchMode->currentPatch() == 0) {
    if (event->firstPressed == 1) {
      this->hx->pressFS4();
    } else {
      this->hx->pressFS5();
    }
  } 
  if (this->patchMode->currentPatch() == 1) {
    if (event->firstPressed == 1) {
      this->hx->nextSnapshot();
    } else {
      this->hx->previousSnapshot();
    }
  }   

  if (this->patchMode->currentPatch() == 2) {
    if (event->firstPressed == 1) {
      this->hx->footswitchModeNext();
    } else {
      this->hx->footswitchModePrevious();
    }
  }   

   if (this->patchMode->currentPatch() == 3) {
     if (event->firstPressed == 1) {
        this->hx->bypassAll();
      } else {
        this->hx->bypassOff();
      }
  }   
  //Signal for DAW
  if (this->patchMode->currentPatch() >= 4 && this->patchMode->currentPatch() <= 8) {
    if (event->firstPressed == 1) {
      this->midi->sendCC(100 + this->patchMode->currentPatch(), 0);
    } else {
      this->midi->sendCC(110 + this->patchMode->currentPatch(), 0);
    }
  }   

  if (this->patchMode->currentPatch() == 9) {
    if (event->firstPressed == 1) {
      this->hx->pressExp1();
    } else {
      this->hx->pressExp2();
    }
  }   
}

void ModeCommandMap::show() {
  display->number(patchMode->currentPatch());
  display->led(false);
}

void ModeCommandMap::loop() {}

