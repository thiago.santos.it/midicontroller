/* 
    System Utility Class
    Created by Thiago Medeiros dos Santos 
*/
#include "Arduino.h"

#ifndef System_h
#define System_h

class System {
 
  public: 

    System();
    int freeMemoryInBytes();  
  private:
    void setup();
};

#endif
