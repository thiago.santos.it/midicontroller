/* 
    MIDI Port Controller
    Created by Thiago Medeiros dos Santos 
*/
#include "Arduino.h"
#include "Log.h"

#ifndef MidiPort_h
#define MidiPort_h

#define CHANNEL_1 0
#define CHANNEL_2 1
#define CHANNEL_3 2
#define CHANNEL_4 3
#define CHANNEL_5 4
#define CHANNEL_6 5
#define CHANNEL_7 6
#define CHANNEL_8 7
#define CHANNEL_9 8
#define CHANNEL_10 9
#define CHANNEL_11 10
#define CHANNEL_12 11
#define CHANNEL_13 12
#define CHANNEL_14 13
#define CHANNEL_15 14
#define CHANNEL_16 15

class MidiPort {
 
  public: 
    MidiPort(int channel);
    void sendCC(int command, int value);  
    void sendPC(int command, int value); 
    void sendNoteOn(int command, int value); 
    void sendNoteOff(int command, int value); 
    void sendAftertouch(int command, int value); 
    void sendChannelPressure(int value); 
    void sendPitchBend(int command, int value); 
    void sendNonMusical(int command, int value); 
  private:
    int channel = 0;
    void setup();
    void send(int command, int param, int value);
    int printableChannel();
};

#endif
