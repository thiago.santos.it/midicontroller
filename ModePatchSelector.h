/* 
    Patch selector mode impl
    Created by Thiago Medeiros dos Santos 
*/
#include "Arduino.h"
#include "SwitchControls.h"
#include "DigitDisplay.h"
#include "ModeBase.h"
#include "Log.h"

#ifndef ModePatchSelector_h
#define ModePatchSelector_h

#define MAX_PATCH 9

class ModePatchSelector: public ModeBase {
 
  public: 
    
    ModePatchSelector(DigitDisplay* display);
    ControllerMode mode();
    byte currentPatch();
    bool changeRequested(Event* event);
    void onEnter(Event* event);
    void onExit(Event* event);
    bool onEvent(Event* event); 
    void show();
    void loop();
    
  private:
    byte patch = 0;
    DigitDisplay* display = NULL;

    void setup();
};

#endif
