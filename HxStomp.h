/* 
    HxStomp controller
    Created by Thiago Medeiros dos Santos 
*/
#include "Arduino.h"
#include "MidiPort.h"

#ifndef HxStomp_h
#define HxStomp_h

class HxStomp {
 
  public: 
    HxStomp(MidiPort* midi);
    void pressExp1();  
    void pressExp2();  
    void pressFS1(); 
    void pressFS2(); 
    void pressFS3(); 
    void pressFS4(); 
    void pressFS5(); 
    void looperOverdub(); 
    void looperRecord(); 
    void looperPlay(); 
    void looperStop();
    void looperPlayOnce(); 
    void looperUndo(); 
    void looperRedo(); 
    void looperForward(); 
    void looperReverse(); 
    void looperFullSpeed(); 
    void looperHalfSpeed(); 
    void tapTempo(); 
    void tuner();
    void snapshot(int num);
    void nextSnapshot();
    void previousSnapshot();
    void bypassAll();
    void bypassOff();
    void footswitchMode(int num);
    void footswitchModeNext();
    void footswitchModePrevious();

  private:
    
    MidiPort* midi = NULL;

    void setup();
};

#endif
